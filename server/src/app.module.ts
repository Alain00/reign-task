import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { NewsModule } from './news/news.module';

const { MONGO_URI, MONGO_USER, MONGO_PASS } = process.env;

@Module({
  imports: [
    MongooseModule.forRoot(MONGO_URI, {
      user: MONGO_USER,
      pass: MONGO_PASS,
    }),
    ScheduleModule.forRoot(),
    NewsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
