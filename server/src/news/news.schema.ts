import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type NewDocument = New & Document;

@Schema()
export class New {
  @Prop()
  created_at: string;
  @Prop()
  title: string;
  @Prop()
  url: string;
  @Prop()
  author: string;
  @Prop()
  points: number;
  @Prop()
  story_text: string;
  @Prop()
  story_title: string;
  @Prop()
  story_url: string;
  @Prop()
  objectID: string;
  @Prop()
  deleted: boolean;
}

export const NewsSchema = SchemaFactory.createForClass(New);
