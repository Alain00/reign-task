import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { New, NewsSchema } from './news.schema';
import { NewsService } from './news.service';
import { NewsController } from './news.controller';
import { Request, RequestSchema } from '../request/request.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: New.name, schema: NewsSchema }]),
    MongooseModule.forFeature([{ name: Request.name, schema: RequestSchema }]),
  ],
  controllers: [NewsController],
  providers: [NewsService],
})
export class NewsModule {}
