import { Controller, Delete, Get, Param } from '@nestjs/common';
import { New } from './news.schema';
import { NewsService } from './news.service';

@Controller('news')
export class NewsController {
  constructor(private readonly newsService: NewsService) {}

  @Get()
  async getAll(): Promise<New[]> {
    return this.newsService.findAll();
  }

  @Delete(':id')
  async deleteOne(@Param('id') id: string): Promise<boolean> {
    return this.newsService.deleteOne(id);
  }
}
